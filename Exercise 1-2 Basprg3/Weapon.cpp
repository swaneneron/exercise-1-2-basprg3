#include "Weapon.h"
#include <iostream>
using namespace std;

class Syphon: public Weapon {
public:
	void attack() {
		cout << "The Player uses Syphon!\n he deals -" << dmg << endl;
	}
};

class normAtk: public Weapon {
public:
	void attack() {
		cout << "The player uses Normal Attack! \n It deals -" << dmg << endl;
	}
};

class critAtk: public Weapon {
public:
	void attack() {
		cout << "The Player attacks with a Critical hit!\n It deals -" << dmg << endl;
	}
};

class  vengeance: public Weapon {
public:
	void attack() {
		cout << "The Player attacks with Vengeance! \n It deals -" << dmg << endl;
	}
};

class dopleBlade : public Weapon {
public:
	void attack() {
		cout << "The Player uses Dople Blade! \n It deals -" << dmg << endl;
	}
};