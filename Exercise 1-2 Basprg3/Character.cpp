#include "Character.h"
#include "Weapon.h"
#include <iostream>
#include <vector>
#include <time.h>
using namespace std;

class Player : public Character {
public:

     void attack() {
		 int randIndex = rand() % 5;
		 if (randIndex == 1) {
			 Hp = Hp - atkPow;
			 cout << "The Player attacks with a Normal Attack! \n he deals " << atkPow << " damage!" << endl;
			 cout << "Enemy is left with " << Hp << endl;
		 }
		 if (randIndex == 2) {
			 int crit;
			 crit = rand() % 5 + 1;
			 if (crit == 1) {
				 Hp = Hp - atkPow * 2;
				 cout << "The Player attacks with a Critical Attack! \n he deals " << atkPow << " damage!" << endl;
				 cout << "Enemy is left with " << Hp << endl;
			 }
		 }
		 if (randIndex == 3) {
			 Hp = Hp - atkPow;
			 cout << "The Player attacks with Syphon! \n he deals " << atkPow << " damage!" << endl;
			 cout << "Enemy is left with " << Hp << endl;
		 }
		 if (randIndex == 4) {
			 Hp = Hp - atkPow*3;
			 cout << "The Player attacks with Vengeance! \n he deals " << atkPow << " damage!" << endl;
			 cout << "Enemy is left with " << Hp << endl;
		 }
		 if (randIndex == 5) {
			 Hp = Hp - atkPow;
			 cout << "The Player attacks dople blade ! \n he deals " << atkPow << " damage!" << endl;
			 cout << "Enemy is left with " << Hp << endl;
		 }
		
	}
};
class Enemy : public Character {
public:
	void attack() {
		cout << "The Enemy attacks! \n he deals " << atkPow << " damage!" << endl;
		Hp = Hp - atkPow;
		cout << "Player is left with " << Hp << endl;

	}
};

