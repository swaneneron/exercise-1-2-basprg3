#include <iostream>
#include <string>
#include "Character.h"
#include "Character.cpp"
#include "Weapon.h"
#include "Weapon.cpp"
#include <vector>
using namespace std;

int main() {
	Player n;
	Enemy  j;
	Player* player1 = &n;
	Enemy* enemy1 = &j;
	player1->setAttackPow(30);
	player1->setHp(1000);
	enemy1->setHp(1000);
	enemy1->setAttackPow(10);

	vector<Weapon*> skills;
	skills.push_back(new Syphon());
	skills.push_back(new normAtk());
	skills.push_back(new critAtk());
	skills.push_back(new vengeance());
	skills.push_back(new dopleBlade());
	int randIndex = rand() % 5;
	skills[randIndex]->setWeaponDmg();
	//for (int i = 0; i < skills.size(); i++) {
	//delete skills[i];
	//};
	while (true) {
		n.attack();
		j.attack();
		system("pause");
	}
	
}